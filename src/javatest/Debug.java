package javatest;

public class Debug {
    public static boolean isEnabled() {
        return true;
    }

    public static void log(String s) {
        if (isEnabled()) {
            System.out.println(s);
        }
    }
}
