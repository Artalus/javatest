package javatest;

import javatest.cache.Cache;
import javatest.cache.Item;
import javatest.cache.ItemNotFoundException;

import java.io.IOException;
import java.lang.ref.SoftReference;

public class Main {
    private Cache c;

    public static void main(String[] args) throws
            IOException,
            ClassNotFoundException,
            ItemNotFoundException {
        new Main().workWithCache();
    }

    private void workWithCache() throws
            IOException,
            ClassNotFoundException,
            ItemNotFoundException {
        c = new Cache(3);

        // all 3 should remain in 1LV
        Long i1 = c.createItem();
        SoftReference<Item> obj1 = c.accessItem(i1);
        printItem("i1", obj1);
        Long i2 = c.createItem();
        printItem("i2", i2);
        printItem("i2", i2);
        Long i3 = c.createItem();
        printItem("i3", i3);
        printItem("i3", i3);
        printItem("i3", i3);

        // 1 should dump
        Long i4 = c.createItem();
        printItem("i4", i4);

        // 4 should be accessed 2 times at removal
        c.killItem(c.accessItem(i4).get().uid);

        // no dumps needed
        Long i5 = c.createItem();
        SoftReference<Item> obj5 = c.accessItem(i5);
        printItem("i5", obj5);

        // 5 should be accessed 1 time at removal
        c.killItem(obj5.get().uid);

        // 1 should not be accessible yet
        printItem("i1", obj1);

        // 5 should not be accessible already
        printItem("i5", obj5);

        // 1 should restore
        printItem("i1", i1);
    }

    private void printItem(String name, Long id) throws
            IOException,
            ClassNotFoundException,
            ItemNotFoundException {
        printItem(name, c.accessItem(id));
    }

    private void printItem(String name, SoftReference<Item> ref) throws
            IOException,
            ClassNotFoundException {
        Item i = ref.get();
        System.out.println(String.format("%s : %s", name, (i != null) ? i.toString() :
                "((null))"));
    }
}
