package javatest.cache;

import java.io.Serializable;
import java.util.Random;

/**
 * An object that can be stored in two-level cache.
 */
public class Item implements Serializable {
    private static Long nextItemUid = 0L;
    public final Long uid;
    private Integer value;

    Item() {
        uid = getNextItemUid();
        value = new Random().nextInt();
    }

    private static Long getNextItemUid() {
        return ++nextItemUid;
    }

    @Override
    public String toString() {
        return String.format("[%d : rand%d]", uid, value);
    }
}
