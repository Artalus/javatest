package javatest.cache;

import java.io.*;

/**
 * Dumpfile manager used to serialize and deserialize cache entries to and from files in
 * specified folder. The entries are saved into separate files based on their UIDs.
 */
class Dump {
    private final String folderName;

    Dump(String folderName) {
        if (folderName.endsWith("/"))
            this.folderName = folderName.substring(0, folderName.length() - 2);
        else
            this.folderName = folderName;
    }

    void dumpEntry(CacheEntry entry) throws
            IOException {
        try (FileOutputStream fout = new FileOutputStream(calcFilePath(entry));
             ObjectOutputStream oos = new ObjectOutputStream(fout);
        ) {
            oos.writeObject(entry);
        }
    }

    CacheEntry restoreEntry(Long targetUid) throws
            IOException,
            ClassNotFoundException,
            ItemNotFoundException {
        CacheEntry restored = null;
        String filePath = calcFilePath(targetUid);
        try (FileInputStream fout = new FileInputStream(filePath);
             ObjectInputStream oos = new ObjectInputStream(fout);
        ) {
            restored = (CacheEntry) oos.readObject();
        }

        if (restored == null || !restored.getData().uid.equals(targetUid))
            throw new ItemNotFoundException("Item was already deleted or never created");

        deleteDump(filePath);

        return restored;
    }

    private void deleteDump(String filePath) throws
            IOException {
        if (!new File(filePath).delete())
            throw new IOException("Couldn't remove dump file  ");
    }

    private String calcFilePath(CacheEntry entry) {
        assert entry != null;
        return calcFilePath(entry.getData().uid);
    }

    private String calcFilePath(Long id) {
        assert id != null;
        return String.format("%s/%d.dmp", folderName, id);
    }
}
