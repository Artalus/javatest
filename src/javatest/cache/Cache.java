package javatest.cache;

import javatest.Debug;

import java.io.File;
import java.io.IOException;
import java.lang.ref.SoftReference;
import java.util.HashMap;

/**
 * A two-level cache that holds specified number of items in memory and dumps the rest
 * to filesystem.
 *
 * @apiNote It is possible for item to be dumped to file while there is reference to
 * the item left somewhere. In this case any changes to such reference will not be
 * represented in cache when the object is restored from dump, and the reference itself
 * may be invalidated at next GC pass.
 */
public class Cache {
    private static final String cacheFolderPrefix = "./.cache";

    private final HashMap<Long, CacheEntry> cache;
    private final Dump dump;
    private final int maximalSize;

    /**
     * Create cache with specified capacity
     *
     * @param maximumSize maximum amount of entries to store in memory
     * @throws IOException if an error occurred at creation of cache directory
     */
    public Cache(int maximumSize) throws
            IOException {
        assert maximumSize > 0;

        this.maximalSize = maximumSize;
        this.cache = new HashMap<>(maximumSize);

        String folderPath = calcCacheFolderPath();
        File cacheFolder = new File(folderPath);
        if (!cacheFolder.mkdirs())
            throw new IOException("Couldn't lock javatest.cache folder at " +
                    cacheFolder.getAbsolutePath());
        this.dump = new Dump(folderPath);

        // TODO: may probably conflict if some dumpfile is opened at destruction
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            File[] files = cacheFolder.listFiles();
            if (files != null)
                for (File file : files) {
                    Debug.log(" -- delete " + file.getAbsolutePath());
                    file.delete();
                }
            Debug.log(" -- delete " + cacheFolder.getAbsolutePath());
            cacheFolder.delete();
            Debug.log("! Cache cleaned");
        }));

        Debug.log("! Cache created");
    }

    /**
     * Create a new item.
     * If cash size is overflown, dump least used item from cache to hard disk
     *
     * @return created object UID. You should store this UID instead of item reference
     * @throws IOException if an error occurred at writing item to file
     */
    public Long createItem() throws
            IOException {
        Debug.log(" > Creating item");
        dumpLeastUsedIfNeeded();

        Item i = new Item();
        CacheEntry entry = new CacheEntry(i);

        addEntryToCache(entry);
        Debug.log(" * Created  " + i.toString());
        return i.uid;
    }

    /**
     * Access cached item; search for item in dump files if it is not present in memory.
     * If item is among dumped and cash size is overflown, dump least used item from cache
     * to hard disk
     *
     * @param uid target item uid
     * @return item from cache. You should not store its reference, because it can become
     * invalid if corresponding item is dumped
     * @throws IOException            if an IO error occurred during lookup or dumping
     * @throws ClassNotFoundException if dumped item cannot be represented by classes
     *                                available in program
     * @throws ItemNotFoundException  if item with such uid was not found neither in
     *                                memory nor in dump
     */
    public SoftReference<Item> accessItem(Long uid) throws
            IOException,
            ClassNotFoundException,
            ItemNotFoundException {
        Debug.log(" > Accessing item#" + uid.toString());
        CacheEntry entry = findOnBothLevels(uid)
                .increaseAccessCount();
        Debug.log(" * Accessed entry  " + entry.toString());
        return new SoftReference<>(entry.getData());
    }

    /**
     * Remove item from cache
     *
     * @param uid target item uid
     * @throws IOException            if an IO error occurred during lookup or dumping
     * @throws ClassNotFoundException if dumped item cannot be represented by classes
     *                                available in program
     * @throws ItemNotFoundException  if item with such uid was not found neither in
     *                                memory nor in dump
     */
    public void killItem(Long uid) throws
            IOException,
            ClassNotFoundException,
            ItemNotFoundException {

        Debug.log(" > Killing  item#" + uid.toString());
        if (cache.remove(uid) == null)
            dump.restoreEntry(uid);
        Debug.log(" * Killed");
    }

    private CacheEntry findOnBothLevels(Long uid) throws
            IOException,
            ClassNotFoundException,
            ItemNotFoundException {
        Debug.log("   Searching for item#" + uid.toString());
        CacheEntry entry = cache.get(uid);
        if (entry != null) {
            Debug.log("   Found in RAM");
            return entry;
        }

        dumpLeastUsedIfNeeded();

        entry = dump.restoreEntry(uid);
        Debug.log("   Found on disk");
        addEntryToCache(entry);
        return entry;
    }

    private void addEntryToCache(CacheEntry entry) throws
            IOException {
        assert entry != null;
        assert cache.size() < maximalSize;

        Debug.log(String.format("   Adding entry  %s  (%d/%d)",
                entry.toString(), cache.size(), maximalSize));

        cache.put(entry.getData().uid, entry);
    }

    private void dumpLeastUsedIfNeeded() throws
            IOException {
        if (cache.size() < maximalSize)
            return;

        Debug.log("     Dumping less used entry");
        HashMap.Entry<Long, CacheEntry> toRemove = findLessUsedItem();
        dump.dumpEntry(toRemove.getValue());
        cache.remove(toRemove.getKey());
    }

    private HashMap.Entry<Long, CacheEntry> findLessUsedItem() {
        Long min = Long.MAX_VALUE;
        HashMap.Entry<Long, CacheEntry> result = null;

        for (HashMap.Entry<Long, CacheEntry> pair : cache.entrySet()) {
            CacheEntry e = pair.getValue();
            if (e.getAccessCount() < min) {
                min = e.getAccessCount();
                result = pair;
            }
        }

        assert result != null;

        Debug.log("     Found less used:  " + result.getValue().toString());

        return result;
    }


    private String calcCacheFolderPath() {
        return String.format("%s-%d-%d",
                cacheFolderPrefix, this.hashCode(), System.currentTimeMillis());
    }

}