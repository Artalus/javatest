package javatest.cache;


import java.io.Serializable;

/**
 * A wrapper for object in cache that can be dumped and restored from file.
 */
class CacheEntry implements Serializable {
    private Item data;
    private Long accessCount = 0L;

    CacheEntry(Item data) {
        this.data = data;
    }

    Item getData() {
        return data;
    }

    CacheEntry increaseAccessCount() {
        ++accessCount;
        return this;
    }

    Long getAccessCount() {
        return accessCount;
    }

    @Override
    public String toString() {
        return data.toString() + "//acc=" + accessCount.toString();
    }
}
