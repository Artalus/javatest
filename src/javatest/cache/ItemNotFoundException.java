package javatest.cache;

/**
 * Signals that no item with specified UID was found in dump
 */
public class ItemNotFoundException extends Exception {
    public ItemNotFoundException() {
    }

    ItemNotFoundException(String message) {
        super(message);
    }
}
